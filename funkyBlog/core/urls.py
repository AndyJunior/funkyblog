from django.urls import path
from . import views

app_name = 'core'
urlpatterns = [
    path('', views.index_view, name='index'),
    path('create/category', views.create_category_view, name="create_category"),
    path('delete/category/<int:pk>', views.delete_category_view, name="delete_category"),
    path('update/category/<int:pk>', views.update_category_view, name='update_category')
]
