from django.db import models
from django.core.validators import MinLengthValidator
from django.utils.text import slugify


class Category(models.Model):
    """ Forum category model """
    name = models.CharField('Category name', max_length=16, validators=[MinLengthValidator(3)])
    order_number = models.PositiveSmallIntegerField('Order number', default=0)

    class Meta:
        """ Meta class """
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Topic(models.Model):
    """Topic model"""
    name = models.CharField('Topic', max_length=128, validators=[MinLengthValidator(8)])
    created_at = models.DateTimeField('Created date', auto_now_add=True)
    active = models.BooleanField('Active', default=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT, blank=False, null=True)

    class Meta:
        """Meta Class"""
        verbose_name_plural = 'Topics'
        verbose_name = 'Topic'

    def __str__(self):
        return slugify(self.name)


class Post(models.Model):
    """ Post Model"""
    text = models.TextField('Text')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, blank=False, null=True)

    class Meta:
        """ Meta class"""
        verbose_name_plural = 'Posts'
        verbose_name = 'Post'
