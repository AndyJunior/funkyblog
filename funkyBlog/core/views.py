from django.views.generic import CreateView, ListView, DeleteView, UpdateView
from django.urls import reverse_lazy
from .forms import  CreateCategoryForm
from .models import Category


class IndexView(ListView):
    """ Index View """
    model = Category
    template_name = "index.html"


index_view = IndexView.as_view()


class CreateCategoryView(CreateView):
    """ Create Category View """
    model = Category
    form_class = CreateCategoryForm
    template_name = "create_category.html"
    success_url = reverse_lazy("core:index")


create_category_view = CreateCategoryView.as_view()


class DeleteCategoryView(DeleteView):
    """ Delete Category View"""
    model = Category
    template_name = "delete_category.html"
    success_url = reverse_lazy('core:index')


delete_category_view = DeleteCategoryView.as_view()


class UpdateCategoryView(UpdateView):
    """ Update category View """
    model = Category
    form_class = CreateCategoryForm
    # fields = ['name']
    template_name = "create_category.html"
    success_url = reverse_lazy('core:index')


update_category_view = UpdateCategoryView.as_view()
